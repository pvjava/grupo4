package com.example.apidemo;

public class Sessions {
	private String data;
	private int expires;
	private String session_id;

	public String getData(){
		return data;
	}

	public void setData(String data){
		this.data = data;
	}

	public int getExpires(){
		return expires;
	}

	public void setExpires(int expires){
		this.expires = expires;
	}

	public String getSession_id(){
		return session_id;
	}

	public void setSession_id(String session_id){
		this.session_id = session_id;
	}


}