package com.example.apidemo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

// /* mySQL */
// //import java.io.*;
// import java.sql.*;

// /* JSON */
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


/* Router */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@SpringBootApplication

@RestController

public class DemoApplication {

	/* página stática */
	@RequestMapping("/")
	public ModelAndView paginaEstatica(){
		final ModelAndView modelo = new ModelAndView();
		modelo.setViewName("paginainicio.html");
		return modelo;
	}

	@RequestMapping(value = {"/uno","/otra"}, method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT})
	public String index() {
		return "<h1>SOY INDEX</h1>";
	}

	// @GetMapping("/")
	// public String index() {
	// 	return "<h1>GET - INDEX</h1>";
	// }


	/* actualiza */
	@PutMapping("/actualiza")
	public int actualiza(@RequestParam final int id, final String name) {
		final String query = "UPDATE user SET username = '" + name + "' WHERE user_id = '" + id + "';";
		return DataBase.escribeBaseDatos(query);
	}
	/* /actualiza */


	/* home */
	@GetMapping("/home")
	public String homeInit() {
		final String query = "SELECT * FROM usuarios WHERE 1;";
		return DataBase.consultaBaseDatos(query);
	}

	@PostMapping("/home")
	public String homeSet() {
		return "<h1>POST - Home page</h1>";
	}

	@PutMapping("/home")
	public String homePut() {
		return "<h1>PUT - Home page</h1>";
	}

	@DeleteMapping("/home")
	public String homeDelete() {
		return "<h1>DELETE - Home page</h1>";
	}
	/* /home */

	/* usuario */
	@GetMapping("/usuario")
	public String usuarioDatos(@RequestParam final int id) {
		String busqueda = "id = '" + id + "'";
		if(id == 0){
			busqueda = "1";
		}
		final String query = "SELECT * FROM usuarios WHERE " + busqueda + ";";
		return DataBase.consultaBaseDatos(query);
	}

	@RequestMapping(value = "/login",  method = RequestMethod.POST)
	public String usuarioRegistro(@RequestBody final Login datos) {
		// Sin cifrado
		 final String query = "SELECT * FROM usuarios WHERE emilio='" + datos.getUsuario() + "' AND clave='" + datos.getContrasena() + "'";
		 final String rta = DataBase.consultaBaseDatos(query);
		 //rta = eliminaCampo("password", rta);
		 System.out.println(rta);
		 return rta;

		//Con cifrado de pass
		//String cifrado = Cifrado.cifra(datos.getContrasena());
		//String query = "SELECT * FROM usuarios WHERE emilio='" + datos.getUsuario() + "' AND clave='" + datos.getContrasena() + "'";
		//String rta = DataBase.consultaBaseDatos(query);

		//rta = eliminaCampo("password", rta);
		//return rta;
	}

	@RequestMapping(value = "/crearoferta",  method = RequestMethod.POST,headers = "Accept=application/json")
	public int crearoferta(@RequestBody final Ofertas datos){ //Se mete el archivo Java que alamcena el formulario
		System.out.println(datos);
		final String campos ="( titulo, fecha_publicacion, sector, provincia, experiencia, tipodecontrato, jornada, rango_salarial, imagen, descripcion)"; //campos de la tabla de la base de datos Mysql (tantos como campos)
		final String valores = "('"+datos.getTitulo()+ "','" + datos.getFecha_publicacion() + "','" + datos.getSector() + "','" + datos.getProvincia() + "','" + datos.getExperiencia() + "','" + datos.getTipodecontrato() + "','" + datos.getJornada() + "','" + datos.getRango_salarial() + "','" + datos.getImagen() + "','" + datos.getDescripcion() + "')"; //tantos get como campos rellenamos
			final String query = "INSERT INTO ofertas " + campos + " VALUES " + valores;
			System.out.println(query);
			return DataBase.escribeBaseDatos(query);
		}

		@RequestMapping(value = "/crearCV",  method = RequestMethod.POST,headers = "Accept=application/json")
	public int crearCV(@RequestBody final CuestionarioCV datos){ //Se mete el archivo Java que alamcena el formulario
		System.out.println(datos);
		final String campos ="( nombrecv, experienciaprevia, puesto, conocimientosytecnologias, fechainicioexperiencia, fechafinexperiencia, tienesestudios, nivel, especialidad, centro, fechainicioestudios, fechafinestudios, presentacion, vehiculo)"; //campos de la tabla de la base de datos Mysql (tantos como campos)
		final String valores = "('" + datos.getNombreCV()+ "','" + datos.getExperienciaPrevia() + "','" + datos.getPuestoTrabajo() + "','" + datos.getConocimientos() + "','" + datos.getFechaIniExp() + "','" + datos.getFechaFinExp() + "','" + datos.getEstudios() + "','" + datos.getNivel() + "','" + datos.getEspecialidad() + "','" + datos.getCentro() + "','" + datos.getFechaIniEstu() + "','" + datos.getFechaFinEstu() + "','" + datos.getPresentacion() + "','" + datos.getVehiculo() + "')"; //tantos get como campos rellenamos
			final String query = "INSERT INTO cvs " + campos + " VALUES " + valores;
			System.out.println(query);
			return DataBase.escribeBaseDatos(query);
		}


	@RequestMapping(value = "/registro",  method = RequestMethod.POST)
	public int registro(@RequestBody final Alumnos datos){ //Se mete el archivo Java que alamcena el formulario
		String query = "SELECT * FROM usuarios WHERE emilio = '" + datos.getEmail() + "'"; //Comprueba si email existe(no siempre es necesario)
		final String rta = DataBase.consultaBaseDatos(query);
		System.out.println(rta);
		if(rta.length()<=2){
			//String cifrado = Cifrado.cifra(datos.getPassword()); //Cifrado quitado (iría donde el getPassword)
			final String campos ="(emilio, clave)"; //campos de la tabla de la base de datos Mysql (tantos como campos)
			final String valores = "('" + datos.getEmail() + "','" + datos.getPassword() + "')"; //tantos get como campos rellenamos
			query = "INSERT INTO usuarios " + campos + " VALUES " + valores;
			System.out.println(query);
			return DataBase.escribeBaseDatos(query);
		}else{
			return -2; //Error de email ya utilizado		
		}
	}

	public String eliminaCampo(final String campo, String texto){
		final String pattern1 = ",\"" + campo + "\":([\"\'])(.*?)\\1"; // ,"campo":"valor"
		final String pattern2 = "\"" + campo + "\":([\"\'])(.*?)\\1,"; // "campo":"valor",
		String pattern;
		final Pattern pat = Pattern.compile(pattern1);
		final Matcher match = pat.matcher(texto);
		if(match.find()){
			pattern = pattern1;
		}else{
			pattern = pattern2;
		}

		texto = texto.replaceAll(pattern,"");
		return texto;
	}

	public static void main(final String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
	}

}




