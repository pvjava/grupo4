package com.example.apidemo;

/* mySQL */
//import java.io.*;
import java.sql.*;

/* JSON */
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/* Generador clases*/
import java.util.Arrays;

/* escritura  de archivos*/
import java.io.FileWriter; 
import java.io.IOException;

public class DataBase implements sqlConfig{
    public static JSONArray convertToJSON(ResultSet resultSet) throws Exception {
        JSONArray jsonArray = new JSONArray();
        if (resultSet != null) {
            while (resultSet.next()) {
                ResultSetMetaData metaData = resultSet.getMetaData();
                int total_rows = metaData.getColumnCount();
                JSONObject obj = new JSONObject();
                for (int i = 0; i < total_rows; i++) {
                    String rKey = metaData.getColumnLabel(i + 1).toLowerCase();
                    Object rVal = resultSet.getObject(i + 1);
                    obj.put(rKey, rVal);
                }
                jsonArray.add(obj);
            }
        }
        return jsonArray;
    }

    static String consultaBaseDatos(String query) {
        String resultado = "";
        try {
            Class.forName(driverJDBC);
            Connection conexion = DriverManager.getConnection("jdbc:mysql://" + server + ":3306/" + baseDatos
                    + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                    user, password);
            Statement st = conexion.createStatement();
			ResultSet rs = st.executeQuery(query);
			resultado = convertToJSON(rs).toString();
            rs.close();
            st.close();
        } catch (SQLException s) {
            System.out.println("Error: SQL.");
            System.out.println("SQLException: " + s.getMessage());
            resultado = "Error SQL";
        } catch (Exception s) {
            System.out.println("Error: Varios.");
            System.out.println("Exception: " + s.getMessage());
            resultado = "Error Varios";
        }
        return resultado;
    }

    static int escribeBaseDatos(String query) {
        int resultado = 0;
        try {
            Class.forName(driverJDBC);
            Connection conexion = DriverManager.getConnection("jdbc:mysql://" + server + ":3306/" + baseDatos
                    + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                    user, password);
			Statement st = conexion.createStatement();
			resultado = st.executeUpdate(query);  
            st.close();
        } catch (SQLException s) {
            System.out.println("Error: SQL.");
            System.out.println("SQLException: " + s.getMessage());
            resultado = 9999999;
        } catch (Exception s) {
            System.out.println("Error: Varios.");
            System.out.println("Exception: " + s.getMessage());
            resultado = 9999998;
        }
        return resultado;
    } 

    // Genera las clases asociadas a las tablas de una base de datos para su uso en
    // recepción de formularios y tratamiento de sus datos.
    // Lo hace con todas las tablas incluidas en la base de datos defida por sqlConfig
    static void GeneraInterfaces() {
        String query;
        String salida = "";
        String nombreClase = "";
        String setget = "";
        String nombreFuncion = "";
        try {
            // nombre del paquete
            DataBase uno = new DataBase();
            Package pack = uno.getClass().getPackage();
            
            // Conexion a la base de datos
            Class.forName(driverJDBC);
            Connection conexion = DriverManager.getConnection("jdbc:mysql://" + server + ":3306/" + baseDatos
                    + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                    user, password);
            Statement st = conexion.createStatement();

            //busca los nombres de todas las tablas
            query = "select table_name from information_schema.tables where table_schema = '" + baseDatos + "'";
            ResultSet tablas = st.executeQuery(query);
            if (tablas != null) {
                while (tablas.next()) {
                    salida = "package " + pack.getName() + ";\r\n\r\n";
                    //ResultSetMetaData metaDataTablas = tablas.getMetaData();
                    Object tabla = tablas.getObject(1);
                    nombreClase = tabla.toString().substring(0, 1).toUpperCase() + tabla.toString().substring(1);
                    salida += "public class " + nombreClase + " {\r\n";
                    //busca los campos de cada tabla
                    query = "select column_name, data_type from information_schema.columns where table_schema = '" + baseDatos + "' and table_name='" + tabla + "'" ;
                    Statement st1 = conexion.createStatement();
                    ResultSet campos = st1.executeQuery(query); 
                    while (campos.next()) {
                        Object campo = campos.getObject(1);
                        Object tipo = campos.getObject(2);
                        
                        //System.out.println("--- El campo es: " + campo.toString().toLowerCase() + " del tipo: " + tipo);
                        // Si no es numerico le asigna String
                        String[] noString = new String[] {"int","short","long","float","double","boolean"};
                        if(!Arrays.asList(noString).contains(tipo)){
                            tipo = "String";
                        };
                        salida += "\tprivate " + tipo + " " + campo + ";\r\n";
                        nombreFuncion = campo.toString().substring(0, 1).toUpperCase() + campo.toString().substring(1);
                        setget += "\tpublic " + tipo + " get" + nombreFuncion + "(){\r\n";
                        setget += "\t\treturn " + campo + ";\r\n";
                        setget += "\t}\r\n\r\n";
                        setget += "\tpublic void set"+ nombreFuncion + "(" + tipo + " " + campo +"){\r\n";
                        setget += "\t\tthis." + campo + " = " + campo + ";\r\n";
                        setget += "\t}\r\n\r\n";
                        
                    }
                    campos.close(); 
                    salida += "\r\n" + setget + "\r\n}";
                    setget = "";
                    escribirArchivo(nombreClase + ".java", salida, false);
                    //System.out.println("La salida: \r\n" + salida);                     
                }
                tablas.close();
            }
            
        } catch (SQLException s) {
            System.out.println("Error: SQL.");
            System.out.println("SQLException: " + s.getMessage());          
        } catch (Exception s) {
            System.out.println("Error: Varios.");
            System.out.println("Exception: " + s.getMessage());            
        }        
    }

    // Escribe en disco el archivo de nombre filemane con el contenido indicado por el segundo parámetro
    // El tercer parámetro (append) añade el contenido al archivo si este ya existía (true)
    // o elimina cualquier contenido previo y lo sustituye por el nuevo.
    public static void escribirArchivo(String filename, String contenido, boolean append){
        try {
            FileWriter myObj = new FileWriter(filename,append);
            myObj.write(contenido);
            myObj.close();
            System.out.println("Archivo " + filename + " creado con éxito");
        } catch (IOException e) {
            System.out.println("Ha ocurrido un error de escritura al crear el archivo " + filename);
            e.printStackTrace();
        }
    }

}