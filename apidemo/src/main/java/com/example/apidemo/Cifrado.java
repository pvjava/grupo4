package com.example.apidemo;

import org.mindrot.jbcrypt.BCrypt;

public class Cifrado {
    public static String cifra(String pass){
        return BCrypt.hashpw(pass, BCrypt.gensalt(10));
    }

    public static boolean comprueba(String pass, String cifrado){
        return BCrypt.checkpw(pass, cifrado);
    }

    public static void democifrado(){
        System.out.println("Demo de cifrado de password:");
        String pass = "123456";
        String cifrado = cifra(pass);
        System.out.println(pass + " --> " + cifrado);
        System.out.println("Cifrado correcto: " + pass + " --> " + cifrado + " -->" + comprueba("123456",cifrado));
        System.out.println("Cifrado incorrecto: " + "12345" + " --> " + cifrado + " -->" + comprueba("12345",cifrado));
        
        for(int i=0; i<10; i++){
            cifrado = cifra(pass);
            System.out.println("Resultado: " + pass + " --> " + cifrado + " -->" + comprueba(pass,cifrado));
        }
    
    }
}