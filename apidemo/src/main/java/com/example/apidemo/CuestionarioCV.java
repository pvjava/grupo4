package com.example.apidemo;

import java.sql.Date;

/*Variables Formulario*/

public class CuestionarioCV {
    private int id;
    private int idSolicitante;
    private String nombreCV;
    private String experienciaPrevia;
    private String puestoTrabajo;
    private String conocimientos;
    private Date fechaIniExp;
    private Date fechaFinExp; 
    private String estudios;
    private String nivel;
    private String especialidad;
    private String centro;
    private Date fechaIniEstu;
    private Date fechaFinEstu;
    private String presentacion;
    private String vehiculo;


    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id = id;
    }

    public int getIdSolicitante(){
        return idSolicitante;
    }
    public void setIdSolicitante(int idSolicitante){
        this.idSolicitante = idSolicitante;
    }

    public String getNombreCV(){
        return nombreCV;
    }
    public void setNombreCV(String nombreCV){
        this.nombreCV = nombreCV;
    }

    public String getExperienciaPrevia(){
        return experienciaPrevia;
    }
    public void setExperienciaPrevia(String experienciaPrevia){
        this.experienciaPrevia = experienciaPrevia;
    }

    public String getPuestoTrabajo(){
        return puestoTrabajo;
    }
    public void setPuestoTrabajo(String puestoTrabajo){
        this.puestoTrabajo = puestoTrabajo;
    }

    public String getConocimientos(){
        return conocimientos;
    }
    public void setConocimientos(String conocimientos){
        this.conocimientos = conocimientos;
    }

    public Date getFechaIniExp(){
        return fechaIniExp;
    }
    public void setFechaIniExp(Date fechaIniExp){
        this.fechaIniExp = fechaIniExp;
    }

    public Date getFechaFinExp(){
        return fechaFinExp;
    }
    public void setFechaFinExp(Date fechaFinExp){
        this.fechaFinExp = fechaFinExp;
    }

    public String getEstudios(){
        return estudios;
    }
    public void setEstudios(String estudios){
        this.estudios = estudios;
    }

    public String getNivel(){
        return nivel;
    }
    public void setNivel(String nivel){
        this.nivel = nivel;
    }

    public String getEspecialidad(){
        return especialidad;
    }
    public void setEspecialidad(String especialidad){
        this.especialidad = especialidad;
    }

    public String getCentro(){
        return centro;
    }
    public void setCentro(String centro){
        this.centro = centro;
    }

    public Date getFechaIniEstu(){
        return fechaIniEstu;
    }
    public void setFechaIniEstu(Date fechaIniEstu){
        this.fechaIniEstu = fechaIniEstu;
    }

    public Date getFechaFinEstu(){
        return fechaFinEstu;
    }
    public void setFechaFinEstu(Date fechaFinEstu){
        this.fechaFinEstu = fechaFinEstu;
    }

    public String getPresentacion(){
        return presentacion;
    }
    public void setPresentacion(String presentacion){
        this.presentacion = presentacion;
    }

    public String getVehiculo(){
        return vehiculo;
    }
    public void setVehiculo(String vehiculo){
        this.vehiculo = vehiculo;
    }
}