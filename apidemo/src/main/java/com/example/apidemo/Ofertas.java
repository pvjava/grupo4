package com.example.apidemo;

import java.sql.Date;

/*Variables Formulario*/

public class Ofertas {
    private int id;
    private int idempresa;
    private String titulo;
    private Date fecha_publicacion;
    private String sector;
    private String provincia;
    private String experiencia;
    private String tipodecontrato;
    private String jornada; 
    private int rango_salarial;
    
    private String imagen;
    private String descripcion;


    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id = id;
    }

    public String getTitulo(){
        return titulo;
    }
    public void setTitulo(String titulo){
        this.titulo = titulo;
    }

    public int getIdEmpresa(){
        return idempresa;
    }
    public void setIdEmpresa(int idempresa){
        this.idempresa = idempresa;
    }

    public Date getFecha_publicacion(){
        return fecha_publicacion;
    }
    public void setFecha_publicacion(Date fecha_publicacion){
        this.fecha_publicacion = fecha_publicacion;
    }

    public String getSector(){
        return sector;
    }
    public void setSector(String sector){
        this.sector = sector;
    }

    public String getProvincia(){
        return provincia;
    }
    public void setProvincia(String provincia){
        this.provincia = provincia;
    }

    public String getExperiencia(){
        return experiencia;
    }
    public void setExperiencia(String experiencia){
        this.experiencia = experiencia;
    }

    public String getTipodecontrato(){
        return tipodecontrato;
    }
    public void setTipodecontrato(String tipodecontrato){
        this.tipodecontrato = tipodecontrato;
    }

    public String getJornada(){
        return jornada;
    }
    public void setJornada(String jornada){
        this.jornada = jornada;
    }

    public int getRango_salarial(){
        return rango_salarial;
    }
    public void setRango_salarial(int rango_salarial){
        this.rango_salarial = rango_salarial;
    }

    

    public String getImagen(){
        return imagen;
    }
    public void setImagen(String imagen){
        this.imagen = imagen;
    }

    public String getDescripcion(){
        return descripcion;
    }
    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }
}