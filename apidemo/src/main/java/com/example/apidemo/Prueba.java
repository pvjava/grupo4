package com.example.apidemo;

public class Prueba {
	private String Apellidos;
	private String Email;
	private int Identificador;
	private String Nombre;
	private String Telefono;

	public String getApellidos(){
		return Apellidos;
	}

	public void setApellidos(String Apellidos){
		this.Apellidos = Apellidos;
	}

	public String getEmail(){
		return Email;
	}

	public void setEmail(String Email){
		this.Email = Email;
	}

	public int getIdentificador(){
		return Identificador;
	}

	public void setIdentificador(int Identificador){
		this.Identificador = Identificador;
	}

	public String getNombre(){
		return Nombre;
	}

	public void setNombre(String Nombre){
		this.Nombre = Nombre;
	}

	public String getTelefono(){
		return Telefono;
	}

	public void setTelefono(String Telefono){
		this.Telefono = Telefono;
	}


}