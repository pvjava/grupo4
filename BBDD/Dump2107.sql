-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: lanapply
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comunidades`
--

DROP TABLE IF EXISTS `comunidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comunidades` (
  `id_comunidad` tinyint NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comunidades`
--

LOCK TABLES `comunidades` WRITE;
/*!40000 ALTER TABLE `comunidades` DISABLE KEYS */;
INSERT INTO `comunidades` VALUES (1,'Andalucía'),(2,'Aragón'),(3,'Asturias, Principado de'),(4,'Balears, Illes'),(5,'Canarias'),(6,'Cantabria'),(7,'Castilla y León'),(8,'Castilla - La Mancha'),(9,'Catalunya'),(10,'Comunitat Valenciana'),(11,'Extremadura'),(12,'Galicia'),(13,'Madrid, Comunidad de'),(14,'Murcia, Región de'),(15,'Navarra, Comunidad Foral de'),(16,'País Vasco'),(17,'Rioja, La'),(18,'Ceuta'),(19,'Melilla'),(1,'Andalucía'),(2,'Aragón'),(3,'Asturias, Principado de'),(4,'Balears, Illes'),(5,'Canarias'),(6,'Cantabria'),(7,'Castilla y León'),(8,'Castilla - La Mancha'),(9,'Catalunya'),(10,'Comunitat Valenciana'),(11,'Extremadura'),(12,'Galicia'),(13,'Madrid, Comunidad de'),(14,'Murcia, Región de'),(15,'Navarra, Comunidad Foral de'),(16,'País Vasco'),(17,'Rioja, La'),(18,'Ceuta'),(19,'Melilla'),(1,'Andalucía'),(2,'Aragón'),(3,'Asturias, Principado de'),(4,'Balears, Illes'),(5,'Canarias'),(6,'Cantabria'),(7,'Castilla y León'),(8,'Castilla - La Mancha'),(9,'Catalunya'),(10,'Comunitat Valenciana'),(11,'Extremadura'),(12,'Galicia'),(13,'Madrid, Comunidad de'),(14,'Murcia, Región de'),(15,'Navarra, Comunidad Foral de'),(16,'País Vasco'),(17,'Rioja, La'),(18,'Ceuta'),(19,'Melilla');
/*!40000 ALTER TABLE `comunidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cvs`
--

DROP TABLE IF EXISTS `cvs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cvs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idsolicitante` int DEFAULT NULL,
  `nombrecv` varchar(45) DEFAULT NULL,
  `experienciaprevia` varchar(45) DEFAULT NULL,
  `puesto trabajo` varchar(45) DEFAULT NULL,
  `conocimientosytecnologias` varchar(45) DEFAULT NULL,
  `fechainicioexperiencia` datetime DEFAULT NULL,
  `fechafinexperiencia` datetime DEFAULT NULL,
  `tienesestudios` varchar(45) DEFAULT NULL,
  `nivel` varchar(45) DEFAULT NULL,
  `especialidad` varchar(45) DEFAULT NULL,
  `centro` varchar(45) DEFAULT NULL,
  `fechainicioestudios` datetime DEFAULT NULL,
  `fechafinestudios` datetime DEFAULT NULL,
  `presentacion` longtext,
  `vehiculo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cv_sol_idx` (`idsolicitante`),
  CONSTRAINT `fk_cv_sol` FOREIGN KEY (`idsolicitante`) REFERENCES `solicitantes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cvs`
--

LOCK TABLES `cvs` WRITE;
/*!40000 ALTER TABLE `cvs` DISABLE KEYS */;
INSERT INTO `cvs` VALUES (1,3,'cv1inigo','si','ingeniero de procesos','mejora continua','2019-04-08 00:00:00','2020-04-08 00:00:00','si','master','ingenieria industrial energetica','escuela de ingenieria de bilbao','2017-10-01 00:00:00','0209-10-01 00:00:00','ingeniero junior deportista con ambicion','si');
/*!40000 ALTER TABLE `cvs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cvs_has_ofertas`
--

DROP TABLE IF EXISTS `cvs_has_ofertas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cvs_has_ofertas` (
  `cvs_id` int NOT NULL,
  `ofertas_id` int NOT NULL,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cvs_id`,`ofertas_id`),
  KEY `fk_cvs_has_ofertas_ofertas1_idx` (`ofertas_id`),
  KEY `fk_cvs_has_ofertas_cvs1_idx` (`cvs_id`),
  CONSTRAINT `fk_cvs_has_ofertas_cvs1` FOREIGN KEY (`cvs_id`) REFERENCES `cvs` (`id`),
  CONSTRAINT `fk_cvs_has_ofertas_ofertas1` FOREIGN KEY (`ofertas_id`) REFERENCES `ofertas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cvs_has_ofertas`
--

LOCK TABLES `cvs_has_ofertas` WRITE;
/*!40000 ALTER TABLE `cvs_has_ofertas` DISABLE KEYS */;
/*!40000 ALTER TABLE `cvs_has_ofertas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresas`
--

DROP TABLE IF EXISTS `empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `empresas` (
  `id` int NOT NULL COMMENT 'Este campo es el mismo valor del id de suauario que se crea.',
  `nombre-usuario` varchar(45) DEFAULT NULL,
  `nombre-empresa` varchar(45) DEFAULT NULL,
  `localizacion` varchar(45) DEFAULT NULL,
  `usuarios_id` int NOT NULL,
  PRIMARY KEY (`id`,`usuarios_id`),
  KEY `fk_empresas_usuarios1_idx` (`usuarios_id`),
  CONSTRAINT `fk_empresas_usuarios1` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresas`
--

LOCK TABLES `empresas` WRITE;
/*!40000 ALTER TABLE `empresas` DISABLE KEYS */;
INSERT INTO `empresas` VALUES (1,'Elena Perez','Empresa1','Bilbao',4),(2,'Carlos Aguirre','Empresa2','Bilbao',5),(3,'Cristina Lopez','Empresa3','Madrid',6);
/*!40000 ALTER TABLE `empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `habilidades`
--

DROP TABLE IF EXISTS `habilidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `habilidades` (
  `id` int NOT NULL AUTO_INCREMENT,
  `habilidad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `habilidades`
--

LOCK TABLES `habilidades` WRITE;
/*!40000 ALTER TABLE `habilidades` DISABLE KEYS */;
/*!40000 ALTER TABLE `habilidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticias`
--

DROP TABLE IF EXISTS `noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `noticias` (
  `idnoticias` int NOT NULL AUTO_INCREMENT,
  `titular` varchar(100) DEFAULT NULL,
  `cuerpo` longtext,
  `imagen` varchar(50) DEFAULT NULL,
  `fuente` varchar(45) DEFAULT NULL,
  `fecha` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idnoticias`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticias`
--

LOCK TABLES `noticias` WRITE;
/*!40000 ALTER TABLE `noticias` DISABLE KEYS */;
INSERT INTO `noticias` VALUES (1,'titular1','cuerpo1','','fuente1','2020-07-02 13:21:47'),(2,'titular2','cuerpo2',NULL,'fuente2','2020-07-02 16:21:47'),(3,'titular3','cuerpo3',NULL,'fuente3','2020-07-03 16:21:47'),(4,'titular4','cuerpo4',NULL,'fuente4','2020-07-01 16:21:47'),(5,'titular5','cuerpo5',NULL,'fuente5','2020-06-30 16:21:47'),(6,'titular6','cuerpo6',NULL,'fuente6','2020-07-03 16:21:47');
/*!40000 ALTER TABLE `noticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ofertas`
--

DROP TABLE IF EXISTS `ofertas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ofertas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idempresa` int DEFAULT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `fecha_publicacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `sector` varchar(45) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  `experiencia` varchar(45) DEFAULT NULL,
  `tipodecontrato` varchar(100) DEFAULT NULL,
  `jornada` varchar(45) DEFAULT NULL,
  `rango_salarial` varchar(45) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `imagen` varchar(45) DEFAULT 'https://picsum.photos/290/190',
  `descripcion` longtext,
  PRIMARY KEY (`id`),
  KEY `fk_ofertas_empresas_idx` (`idempresa`),
  CONSTRAINT `fk_ofertas_empresas` FOREIGN KEY (`idempresa`) REFERENCES `empresas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ofertas`
--

LOCK TABLES `ofertas` WRITE;
/*!40000 ALTER TABLE `ofertas` DISABLE KEYS */;
INSERT INTO `ofertas` VALUES (1,1,'titulo1','2020-04-03 16:52:24','Ingenieria','Bizkaia','Sin experiencia',NULL,'Completa',NULL,NULL,'https://picsum.photos/290/190',NULL),(2,1,'titulo2','2020-05-03 16:52:24','Ingenieria','Bizkaia','Sin experiencia',NULL,'Completa',NULL,NULL,'https://picsum.photos/290/190',NULL),(3,1,'titulo3','2020-06-05 16:52:24','Ingenieria','Bizkaia','Sin experiencia',NULL,'Completa',NULL,NULL,'https://picsum.photos/290/190',NULL),(4,2,'titulo4','2020-06-05 16:57:46','Ingenieria','Bizkaia','Sin experiencia',NULL,'Media jornada',NULL,NULL,'https://picsum.photos/290/190',NULL),(5,3,'titulo5','2020-06-06 16:57:46','Comercial','Bizkaia','1 año de experiencia',NULL,'Completa',NULL,NULL,'https://picsum.photos/290/190',NULL),(6,2,'titulo6','2020-07-03 16:57:46','Comercial','Madrid','Sin experiencia',NULL,'Completa',NULL,NULL,'https://picsum.photos/290/190',NULL),(7,2,'titulo7','2020-07-03 16:57:46','Administration','Madrid','Sin experiencia',NULL,'Media jornada',NULL,NULL,'https://picsum.photos/290/190',NULL),(8,3,'titulo8','2020-07-03 16:57:46','Investigacion','Madrid','1 año de experiencia',NULL,'Completa',NULL,NULL,'https://picsum.photos/290/190',NULL);
/*!40000 ALTER TABLE `ofertas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paises`
--

DROP TABLE IF EXISTS `paises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paises` (
  `id` int NOT NULL AUTO_INCREMENT,
  `iso` char(2) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paises`
--

LOCK TABLES `paises` WRITE;
/*!40000 ALTER TABLE `paises` DISABLE KEYS */;
INSERT INTO `paises` VALUES (1,'AF','Afganistán'),(2,'AX','Islas Gland'),(3,'AL','Albania'),(4,'DE','Alemania'),(5,'AD','Andorra'),(6,'AO','Angola'),(7,'AI','Anguilla'),(8,'AQ','Antártida'),(9,'AG','Antigua y Barbuda'),(10,'AN','Antillas Holandesas'),(11,'SA','Arabia Saudí'),(12,'DZ','Argelia'),(13,'AR','Argentina'),(14,'AM','Armenia'),(15,'AW','Aruba'),(16,'AU','Australia'),(17,'AT','Austria'),(18,'AZ','Azerbaiyán'),(19,'BS','Bahamas'),(20,'BH','Bahréin'),(21,'BD','Bangladesh'),(22,'BB','Barbados'),(23,'BY','Bielorrusia'),(24,'BE','Bélgica'),(25,'BZ','Belice'),(26,'BJ','Benin'),(27,'BM','Bermudas'),(28,'BT','Bhután'),(29,'BO','Bolivia'),(30,'BA','Bosnia y Herzegovina'),(31,'BW','Botsuana'),(32,'BV','Isla Bouvet'),(33,'BR','Brasil'),(34,'BN','Brunéi'),(35,'BG','Bulgaria'),(36,'BF','Burkina Faso'),(37,'BI','Burundi'),(38,'CV','Cabo Verde'),(39,'KY','Islas Caimán'),(40,'KH','Camboya'),(41,'CM','Camerún'),(42,'CA','Canadá'),(43,'CF','República Centroafricana'),(44,'TD','Chad'),(45,'CZ','República Checa'),(46,'CL','Chile'),(47,'CN','China'),(48,'CY','Chipre'),(49,'CX','Isla de Navidad'),(50,'VA','Ciudad del Vaticano'),(51,'CC','Islas Cocos'),(52,'CO','Colombia'),(53,'KM','Comoras'),(54,'CD','República Democrática del Congo'),(55,'CG','Congo'),(56,'CK','Islas Cook'),(57,'KP','Corea del Norte'),(58,'KR','Corea del Sur'),(59,'CI','Costa de Marfil'),(60,'CR','Costa Rica'),(61,'HR','Croacia'),(62,'CU','Cuba'),(63,'DK','Dinamarca'),(64,'DM','Dominica'),(65,'DO','República Dominicana'),(66,'EC','Ecuador'),(67,'EG','Egipto'),(68,'SV','El Salvador'),(69,'AE','Emiratos Árabes Unidos'),(70,'ER','Eritrea'),(71,'SK','Eslovaquia'),(72,'SI','Eslovenia'),(73,'ES','España'),(74,'UM','Islas ultramarinas de Estados Unidos'),(75,'US','Estados Unidos'),(76,'EE','Estonia'),(77,'ET','Etiopía'),(78,'FO','Islas Feroe'),(79,'PH','Filipinas'),(80,'FI','Finlandia'),(81,'FJ','Fiyi'),(82,'FR','Francia'),(83,'GA','Gabón'),(84,'GM','Gambia'),(85,'GE','Georgia'),(86,'GS','Islas Georgias del Sur y Sandwich del Sur'),(87,'GH','Ghana'),(88,'GI','Gibraltar'),(89,'GD','Granada'),(90,'GR','Grecia'),(91,'GL','Groenlandia'),(92,'GP','Guadalupe'),(93,'GU','Guam'),(94,'GT','Guatemala'),(95,'GF','Guayana Francesa'),(96,'GN','Guinea'),(97,'GQ','Guinea Ecuatorial'),(98,'GW','Guinea-Bissau'),(99,'GY','Guyana'),(100,'HT','Haití'),(101,'HM','Islas Heard y McDonald'),(102,'HN','Honduras'),(103,'HK','Hong Kong'),(104,'HU','Hungría'),(105,'IN','India'),(106,'ID','Indonesia'),(107,'IR','Irán'),(108,'IQ','Iraq'),(109,'IE','Irlanda'),(110,'IS','Islandia'),(111,'IL','Israel'),(112,'IT','Italia'),(113,'JM','Jamaica'),(114,'JP','Japón'),(115,'JO','Jordania'),(116,'KZ','Kazajstán'),(117,'KE','Kenia'),(118,'KG','Kirguistán'),(119,'KI','Kiribati'),(120,'KW','Kuwait'),(121,'LA','Laos'),(122,'LS','Lesotho'),(123,'LV','Letonia'),(124,'LB','Líbano'),(125,'LR','Liberia'),(126,'LY','Libia'),(127,'LI','Liechtenstein'),(128,'LT','Lituania'),(129,'LU','Luxemburgo'),(130,'MO','Macao'),(131,'MK','ARY Macedonia'),(132,'MG','Madagascar'),(133,'MY','Malasia'),(134,'MW','Malawi'),(135,'MV','Maldivas'),(136,'ML','Malí'),(137,'MT','Malta'),(138,'FK','Islas Malvinas'),(139,'MP','Islas Marianas del Norte'),(140,'MA','Marruecos'),(141,'MH','Islas Marshall'),(142,'MQ','Martinica'),(143,'MU','Mauricio'),(144,'MR','Mauritania'),(145,'YT','Mayotte'),(146,'MX','México'),(147,'FM','Micronesia'),(148,'MD','Moldavia'),(149,'MC','Mónaco'),(150,'MN','Mongolia'),(151,'MS','Montserrat'),(152,'MZ','Mozambique'),(153,'MM','Myanmar'),(154,'NA','Namibia'),(155,'NR','Nauru'),(156,'NP','Nepal'),(157,'NI','Nicaragua'),(158,'NE','Níger'),(159,'NG','Nigeria'),(160,'NU','Niue'),(161,'NF','Isla Norfolk'),(162,'NO','Noruega'),(163,'NC','Nueva Caledonia'),(164,'NZ','Nueva Zelanda'),(165,'OM','Omán'),(166,'NL','Países Bajos'),(167,'PK','Pakistán'),(168,'PW','Palau'),(169,'PS','Palestina'),(170,'PA','Panamá'),(171,'PG','Papúa Nueva Guinea'),(172,'PY','Paraguay'),(173,'PE','Perú'),(174,'PN','Islas Pitcairn'),(175,'PF','Polinesia Francesa'),(176,'PL','Polonia'),(177,'PT','Portugal'),(178,'PR','Puerto Rico'),(179,'QA','Qatar'),(180,'GB','Reino Unido'),(181,'RE','Reunión'),(182,'RW','Ruanda'),(183,'RO','Rumania'),(184,'RU','Rusia'),(185,'EH','Sahara Occidental'),(186,'SB','Islas Salomón'),(187,'WS','Samoa'),(188,'AS','Samoa Americana'),(189,'KN','San Cristóbal y Nevis'),(190,'SM','San Marino'),(191,'PM','San Pedro y Miquelón'),(192,'VC','San Vicente y las Granadinas'),(193,'SH','Santa Helena'),(194,'LC','Santa Lucía'),(195,'ST','Santo Tomé y Príncipe'),(196,'SN','Senegal'),(197,'CS','Serbia y Montenegro'),(198,'SC','Seychelles'),(199,'SL','Sierra Leona'),(200,'SG','Singapur'),(201,'SY','Siria'),(202,'SO','Somalia'),(203,'LK','Sri Lanka'),(204,'SZ','Suazilandia'),(205,'ZA','Sudáfrica'),(206,'SD','Sudán'),(207,'SE','Suecia'),(208,'CH','Suiza'),(209,'SR','Surinam'),(210,'SJ','Svalbard y Jan Mayen'),(211,'TH','Tailandia'),(212,'TW','Taiwán'),(213,'TZ','Tanzania'),(214,'TJ','Tayikistán'),(215,'IO','Territorio Británico del Océano Índico'),(216,'TF','Territorios Australes Franceses'),(217,'TL','Timor Oriental'),(218,'TG','Togo'),(219,'TK','Tokelau'),(220,'TO','Tonga'),(221,'TT','Trinidad y Tobago'),(222,'TN','Túnez'),(223,'TC','Islas Turcas y Caicos'),(224,'TM','Turkmenistán'),(225,'TR','Turquía'),(226,'TV','Tuvalu'),(227,'UA','Ucrania'),(228,'UG','Uganda'),(229,'UY','Uruguay'),(230,'UZ','Uzbekistán'),(231,'VU','Vanuatu'),(232,'VE','Venezuela'),(233,'VN','Vietnam'),(234,'VG','Islas Vírgenes Británicas'),(235,'VI','Islas Vírgenes de los Estados Unidos'),(236,'WF','Wallis y Futuna'),(237,'YE','Yemen'),(238,'DJ','Yibuti'),(239,'ZM','Zambia'),(240,'ZW','Zimbabue');
/*!40000 ALTER TABLE `paises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `provincias` (
  `id_provincia` smallint DEFAULT NULL,
  `provincia` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provincias`
--

LOCK TABLES `provincias` WRITE;
/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
INSERT INTO `provincias` VALUES (2,'Albacete'),(3,'Alicante/Alacant'),(4,'Almería'),(1,'Araba/Álava'),(33,'Asturias'),(5,'Ávila'),(6,'Badajoz'),(7,'Balears, Illes'),(8,'Barcelona'),(48,'Bizkaia'),(9,'Burgos'),(10,'Cáceres'),(11,'Cádiz'),(39,'Cantabria'),(12,'Castellón/Castelló'),(51,'Ceuta'),(13,'Ciudad Real'),(14,'Córdoba'),(15,'Coruña, A'),(16,'Cuenca'),(20,'Gipuzkoa'),(17,'Girona'),(18,'Granada'),(19,'Guadalajara'),(21,'Huelva'),(22,'Huesca'),(23,'Jaén'),(24,'León'),(27,'Lugo'),(25,'Lleida'),(28,'Madrid'),(29,'Málaga'),(52,'Melilla'),(30,'Murcia'),(31,'Navarra'),(32,'Ourense'),(34,'Palencia'),(35,'Palmas, Las'),(36,'Pontevedra'),(26,'Rioja, La'),(37,'Salamanca'),(38,'Santa Cruz de Tenerife'),(40,'Segovia'),(41,'Sevilla'),(42,'Soria'),(43,'Tarragona'),(44,'Teruel'),(45,'Toledo'),(46,'Valencia/València'),(47,'Valladolid'),(49,'Zamora'),(50,'Zaragoza');
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitantes`
--

DROP TABLE IF EXISTS `solicitantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `solicitantes` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'El código del solicitante es el del usuario creado para ese solicitante. Por tanto, el registro de solictante desde ser creado DESPUES de crear el usuario y obtener su ID',
  `nombre` varchar(45) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `fechanacimiento` date NOT NULL,
  `pais` varchar(45) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  `poblacion` varchar(100) DEFAULT NULL,
  `genero` varchar(15) DEFAULT NULL,
  `usuarios_id` int NOT NULL,
  PRIMARY KEY (`id`,`usuarios_id`),
  KEY `fk_solicitantes_usuarios1_idx` (`usuarios_id`),
  CONSTRAINT `fk_sol_usu` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitantes`
--

LOCK TABLES `solicitantes` WRITE;
/*!40000 ALTER TABLE `solicitantes` DISABLE KEYS */;
INSERT INTO `solicitantes` VALUES (1,'Juanjo','Caralampio Iniesta','1994-12-06','España','Madrid','Madrid','Hombre',1),(2,'Sergio','Vegas Suarez','1992-03-28','España','Bizkaia','Portugalete','Hombre',2),(3,'Iñigo','Ayani Gabilondo','1993-04-03','España','Bizkaia','Getxo','Hombre',3);
/*!40000 ALTER TABLE `solicitantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `emilio` varchar(100) DEFAULT NULL,
  `clave` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emilio_UNIQUE` (`emilio`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'juanjociniesta@gmail.com','Juanjo001'),(2,'svegas004@gmail.com','Sergio001'),(3,'iayanigabilondo@gmail.com','Inigo001'),(4,'empresa1@gmail.com','Empresa001'),(5,'empresa2@gmail.com','Empresa002'),(6,'empresa3@gmail.com','Empresa003'),(7,'carlos@gmail.com','Carlos001');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'lanapply'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-21 16:21:34
